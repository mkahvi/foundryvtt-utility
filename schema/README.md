# Schema

## Files

### manifest.json

Schema for `module.json`, `system.json` and `world.json` manifests.

### template.json

Schema for system `template.json`

## Instructions

### VScode

In User Settings JSON, you can configure usage as so:

```json
"json.schemas": [
{
	"fileMatch": ["module.json", "system.json", "world.json"],
	"url": "https://gitlab.com/mkahvi/foundryvtt-utility/-/raw/main/schema/v11/manifest.json"
},
{
	"fileMatch": ["template.json"],
	"url": "https://gitlab.com/mkahvi/foundryvtt-utility/-/raw/main/schema/v10/template.json"
}
],
```
